package com.inkwell.portlet;

import java.io.IOException;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.inkwell.service.model.Person;
import com.inkwell.service.model.Registration;
import com.inkwell.service.model.impl.PersonImpl;
import com.inkwell.service.model.impl.RegistrationImpl;
import com.inkwell.service.service.PersonLocalServiceUtil;
import com.inkwell.service.service.RegistrationLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Address;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.AddressLocalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;

@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.name=com_inkwell_portlet_ProdregPortlet",
		"javax.portlet.display-name=prodreg-web Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class ProdregPortlet extends MVCPortlet {
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		Person person = new PersonImpl();
		Registration registration = new RegistrationImpl();
		ThemeDisplay themeDisplay = ActionUtil.getThemeDisplay(renderRequest);
		
		if (themeDisplay.isSignedIn()) {
			User user = themeDisplay.getUser();
			List<Address> addresses = AddressLocalServiceUtil.getAddresses(themeDisplay.getCompanyId(), User.class.getName(), user.getUserId());
			Address homeAddr = (addresses.isEmpty()) ? null : addresses.get(0);
			
			if (homeAddr != null) {
				person.setAddress1(homeAddr.getStreet1());
				person.setAddress2(homeAddr.getStreet2());
				person.setCity(homeAddr.getCity());
				person.setPostalCode(homeAddr.getZip());
				person.setCountry(homeAddr.getCountry().toString());
			}
			
			person.setLastName(user.getLastName());
			person.setFirstName(user.getFirstName());
			person.setEmail(user.getEmailAddress());
			
			try {
				person.setBirthDate(user.getBirthday());
				person.setMale(user.isMale());
			} catch (PortalException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		renderRequest.setAttribute("person", person);
		renderRequest.setAttribute("registration", registration);
		
		super.render(renderRequest, renderResponse);
	}
	
	public void registerProduct(ActionRequest actionRequest, ActionResponse actionResponse) throws PortalException {
		Person person = ActionUtil.getPersonFromRequest(actionRequest);
		Registration registration = ActionUtil.getRegistrationFromRequest(actionRequest);
		ThemeDisplay themeDisplay = ActionUtil.getThemeDisplay(actionRequest);
		long userId = themeDisplay.getUserId();
		
		person = PersonLocalServiceUtil.addPerson(person, userId);
		
		registration.setPersonId(person.getId());
		registration = RegistrationLocalServiceUtil.addRegistration(registration, userId);
	}
}
