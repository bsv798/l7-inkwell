package com.inkwell.portlet;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.portlet.PortletRequest;

import com.inkwell.service.model.Person;
import com.inkwell.service.model.Registration;
import com.inkwell.service.model.impl.PersonImpl;
import com.inkwell.service.model.impl.RegistrationImpl;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.CalendarFactoryUtil;
import com.liferay.portal.kernel.util.DateFormatFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;

public class ActionUtil {
	private static final String gp = "prodreg-web.gender.item.";
	private static final List<String> genderList = Arrays.asList(new String[] {
			gp + "male", gp + "female" });

	private static final String hp = "prodreg-web.how-hear.item.";
	private static final List<String> howHearList = Arrays.asList(new String[] {
			hp + "tv-advertisement", hp + "radio-advertisement", hp + "tv-news", hp + "magazine-article", hp + "retail-store", 
			hp + "friend-family-member", hp + "inkwell.com", hp + "other-web-site", hp + "trade-show", hp + "home-shopping" });

	private static final String wp = "prodreg-web.where-purchase.item.";
	private static final List<String> wherePurchaseList = Arrays.asList(new String[] {
			wp + "retail-store", wp + "tv-shopping-network", wp + "gift", wp + "catalog", wp + "online-retailer",
			wp + "inkwell.com", wp + "other" });
	
	public static Calendar getCalendarWithDate(Date date) {
		Calendar cal = CalendarFactoryUtil.getCalendar();
		
		if (date != null) {
			cal.setTime(date);
		}
		
		return cal;
	}

	public static ThemeDisplay getThemeDisplay(PortletRequest request) {
		return (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}
	
	public static List<String> getGenderList() {
		return genderList;
	}
	
	public static List<String> getHowHearList() {
		return howHearList;
	}

	public static List<String> getWherepurchaselist() {
		return wherePurchaseList;
	}
	
	public static Person getPersonFromRequest(PortletRequest request) {
		ThemeDisplay themeDisplay = getThemeDisplay(request);
		Person person = new PersonImpl();
		
		person.setCompanyId(themeDisplay.getCompanyId());
		person.setGroupId(themeDisplay.getScopeGroupId());
		person.setFirstName(ParamUtil.getString(request, "firstName"));
		person.setLastName(ParamUtil.getString(request, "lastName"));
		person.setAddress1(ParamUtil.getString(request, "address1"));
		person.setAddress2(ParamUtil.getString(request, "address2"));
		person.setCity(ParamUtil.getString(request, "city"));
		person.setState(ParamUtil.getString(request, "state"));
		person.setPostalCode(ParamUtil.getString(request, "postalCode"));
		person.setPhoneNumber(ParamUtil.getString(request, "phoneNumber"));
		person.setCountry(ParamUtil.getString(request, "country"));
		person.setEmail(ParamUtil.getString(request, "email"));
		person.setMale(ParamUtil.getString(request, "gender").equalsIgnoreCase("male"));
		person.setBirthDate(ParamUtil.getDate(request, "birthDate", DateFormatFactoryUtil.getDate(themeDisplay.getLocale())));
		
		return person;
	}
	
	public static Registration getRegistrationFromRequest(PortletRequest request) {
		ThemeDisplay themeDisplay = getThemeDisplay(request);
		Registration registration = new RegistrationImpl();
		
		registration.setCompanyId(themeDisplay.getCompanyId());
		registration.setGroupId(themeDisplay.getScopeGroupId());
		registration.setHowHear(ParamUtil.getString(request, "howHear"));
		registration.setProductId(ParamUtil.getLong(request, "productType"));
		registration.setSerialNumber(ParamUtil.getString(request, "productSerialNumber"));
		registration.setWherePurchased(ParamUtil.getString(request, "wherePurchase"));
		registration.setDatePurchased(ParamUtil.getDate(request, "datePurchased", DateFormatFactoryUtil.getDate(themeDisplay.getLocale())));
		
		return registration;
	}
}
