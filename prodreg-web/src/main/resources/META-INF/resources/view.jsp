<%@page import="com.inkwell.service.model.Product"%>
<%@page import="com.inkwell.service.service.ProductLocalServiceUtil"%>
<%@page import="com.inkwell.service.model.Person"%>
<%@page import="com.inkwell.service.model.Registration"%>
<%@page import="java.util.Date"%>
<%@page import="com.inkwell.portlet.ActionUtil"%>
<%@ include file="init.jsp" %>

<jsp:useBean id="person" type="com.inkwell.service.model.Person" scope="request" />
<jsp:useBean id="registration" type="com.inkwell.service.model.Registration" scope="request" />

<portlet:actionURL name="registerProduct" var="registerProductURL" />

<aui:form name="fm" action="<%= registerProductURL %>" method="post">
	<aui:fieldset>
		<aui:input name="firstName" label="prodreg-web.first-name" required="true" value="<%= person.getFirstName() %>" />
		<aui:input name="lastName" label="prodreg-web.last-name" required="true" value="<%= person.getLastName() %>" />
		<aui:input name="address1" label="prodreg-web.address1" value="<%= person.getAddress1() %>" />
		<aui:input name="address2" label="prodreg-web.address2" value="<%= person.getAddress2() %>" />
		<aui:input name="city" label="prodreg-web.city" required="true" value="<%= person.getCity() %>" />
		<aui:input name="state" label="prodreg-web.stae" required="true" value="<%= person.getState() %>" />
		<aui:input name="postalCode" label="prodreg-web.postal_code" required="true" value="<%= person.getPostalCode() %>" />
		<aui:input name="country" label="prodreg-web.country" required="true" value="<%= person.getCountry() %>" />
		<aui:input name="email" label="prodreg-web.email" type="email" required="true" value="<%= person.getEmail() %>" />
		<aui:input name="phoneNumber" label="prodreg-web.phone-number" required="true" value="<%= person.getPhoneNumber() %>" />
		<aui:select name="gender" label="prodreg-web.gender" required="true">
			<aui:option label="prodreg-web.please-choose" value="" />
			<% for (String item : ActionUtil.getGenderList()) { %>
				<aui:option label="<%= item %>" />
			<% } %>
		</aui:select>
		<aui:input name="birthDate" label="prodreg-web.birth-date" required="true" model="<%= Person.class %>" bean="<%= person %>" value="<%= ActionUtil.getCalendarWithDate(person.getBirthDate()) %>" />
		<aui:select name="howHear" label="prodreg-web.how-hear" required="true">
			<aui:option label="prodreg-web.please-choose" value="" />
			<% for (String item : ActionUtil.getHowHearList()) { %>
				<aui:option label="<%= item %>" />
			<% } %>
		</aui:select>
		<aui:select name="wherePurchase" label="prodreg-web.where-purchase" required="true">
			<aui:option label="prodreg-web.please-choose" value="" />
			<% for (String item : ActionUtil.getWherepurchaselist()) { %>
				<aui:option label="<%= item %>" />
			<% } %>
		</aui:select>
		<aui:input name="datePurchased" label="prodreg-web.date-purchased" required="true" model="<%= Registration.class %>" bean="<%= registration %>" value="<%= ActionUtil.getCalendarWithDate(new Date()) %>" />
		<aui:select name="productType" label="prodreg-web.product-type" required="true">
			<aui:option label="prodreg-web.please-choose" value="" />
			<% for (Product item : ProductLocalServiceUtil.getAllProductsByGroupId(themeDisplay.getSiteGroupId())) { %>
				<aui:option label="<%= item.getName() %>" value="<%= item.getId() %>" />
			<% } %>
		</aui:select>
		<aui:input name="productSerialNumber" label="prodreg-web.product-serial-number" required="true" />
		
		<aui:button type="submit" />
	</aui:fieldset>
</aui:form>
