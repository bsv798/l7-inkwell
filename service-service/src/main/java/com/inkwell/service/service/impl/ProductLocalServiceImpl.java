/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.inkwell.service.service.impl;

import aQute.bnd.annotation.ProviderType;

import java.util.List;

import com.inkwell.service.model.Product;
import com.inkwell.service.service.base.ProductLocalServiceBaseImpl;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.ResourceConstants;

/**
 * The implementation of the product local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.inkwell.service.service.ProductLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author bsv798
 * @see ProductLocalServiceBaseImpl
 * @see com.inkwell.service.service.ProductLocalServiceUtil
 */
@ProviderType
public class ProductLocalServiceImpl extends ProductLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.inkwell.service.service.ProductLocalServiceUtil} to access the product local service.
	 */
	
	@Override
	public Product addProduct(Product product, long userId) throws PortalException {
		long productId = counterLocalService.increment(Product.class.getName());
		
		product.setPrimaryKey(productId);
		
		resourceLocalService.addResources(product.getCompanyId(), product.getGroupId(), userId, 
				Product.class.getName(), productId, false, true, true);
		
		return super.addProduct(product);
	}
	
	@Override
	public Product deleteProduct(long productId) throws PortalException {
		return deleteProduct(productPersistence.findByPrimaryKey(productId));
	}
	
	@Override
	public Product deleteProduct(Product product) throws PortalException {
		resourceLocalService.deleteResource(product.getCompanyId(), Product.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL, product.getPrimaryKey());
		
		return super.deleteProduct(product);
	}
	
	@Override
	public List<Product> getAllProductsByGroupId(long groupId) {
		return productPersistence.findByGroupId(groupId);
	}
}
