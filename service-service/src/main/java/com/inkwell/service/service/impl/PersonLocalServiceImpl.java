/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.inkwell.service.service.impl;

import aQute.bnd.annotation.ProviderType;

import java.util.List;

import com.inkwell.service.model.Person;
import com.inkwell.service.service.base.PersonLocalServiceBaseImpl;
import com.inkwell.service.service.persistence.PersonPersistence;
import com.inkwell.service.service.persistence.impl.PersonPersistenceImpl;
import com.liferay.portal.kernel.exception.PortalException;

/**
 * The implementation of the person local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.inkwell.service.service.PersonLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author bsv798
 * @see PersonLocalServiceBaseImpl
 * @see com.inkwell.service.service.PersonLocalServiceUtil
 */
@ProviderType
public class PersonLocalServiceImpl extends PersonLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.inkwell.service.service.PersonLocalServiceUtil} to access the person local service.
	 */
	
	@Override
	public Person addPerson(Person person, long userId) throws PortalException {
		long personId = counterLocalService.increment(Person.class.getName());
		
		person.setPrimaryKey(personId);
		person.setUserId(userId);
		
		resourceLocalService.addResources(person.getCompanyId(), person.getGroupId(), Person.class.getName(), false);
		
		return super.addPerson(person);
	}
	
	@Override
	public Person getPerson(long groupId, long userId) {
		List<Person> personList = personPersistence.findByGroupIdUserId(groupId, userId, 0, 1);
		
		if (!personList.isEmpty()) {
			return personList.get(0);
		} else {
			return null;
		}
	}
}
