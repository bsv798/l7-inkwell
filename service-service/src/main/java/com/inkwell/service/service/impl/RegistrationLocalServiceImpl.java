/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.inkwell.service.service.impl;

import aQute.bnd.annotation.ProviderType;

import java.util.List;

import com.inkwell.service.model.Registration;
import com.inkwell.service.service.base.RegistrationLocalServiceBaseImpl;
import com.liferay.portal.kernel.exception.PortalException;

/**
 * The implementation of the registration local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.inkwell.service.service.RegistrationLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author bsv798
 * @see RegistrationLocalServiceBaseImpl
 * @see com.inkwell.service.service.RegistrationLocalServiceUtil
 */
@ProviderType
public class RegistrationLocalServiceImpl
	extends RegistrationLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.inkwell.service.service.RegistrationLocalServiceUtil} to access the registration local service.
	 */
	
	@Override
	public Registration addRegistration(Registration registration, long userId) throws PortalException {
		long registrationId = counterLocalService.increment(Registration.class.getName());
		
		registration.setPrimaryKey(registrationId);
		
		resourceLocalService.addResources(registration.getCompanyId(), registration.getGroupId(), Registration.class.getName(), false);
		
		return super.addRegistration(registration);
	}
	
	@Override
	public List<Registration> getAllRegistrationsByGroupId(long groupId) {
		return registrationPersistence.findByGroupId(groupId);
	}
}
