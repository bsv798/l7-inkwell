<%@ include file="init.jsp" %>

<%
ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
Product product = (Product) row.getObject();
long groupId = themeDisplay.getLayout().getGroupId();
String name = Product.class.getName();
String primKey = String.valueOf(product.getPrimaryKey());
%>

<liferay-ui:icon-menu>
	<c:if test="<%= permissionChecker.hasPermission(groupId, name, primKey, ActionKeys.UPDATE) %>">
		<portlet:actionURL name="editProduct" var="editURL">
			<portlet:param name="resourcePrimKey" value="<%= primKey %>" />
		</portlet:actionURL>
		<liferay-ui:icon image="edit" url="<%= editURL %>" />
	</c:if>
	
	<c:if test="<%= permissionChecker.hasPermission(groupId, name, primKey, ActionKeys.DELETE) %>">
		<portlet:actionURL name="deleteProduct" var="deleteURL">
			<portlet:param name="resourcePrimKey" value="<%= primKey %>" />
		</portlet:actionURL>
		<liferay-ui:icon-delete image="delete" url="<%= deleteURL %>" />
	</c:if>


	<c:if test="<%= permissionChecker.hasPermission(groupId, name, primKey, ActionKeys.PERMISSIONS) %>">
		<liferay-security:permissionsURL modelResource="<%= Product.class.getName() %>" modelResourceDescription="<%= product.getName() %>" resourcePrimKey="<%= primKey %>" var="permissionsURL" />
		<liferay-ui:icon image="permissions" url="<%= permissionsURL %>" />
	</c:if>
</liferay-ui:icon-menu>