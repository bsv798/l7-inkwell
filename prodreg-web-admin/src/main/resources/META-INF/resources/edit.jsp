<%@ include file="init.jsp" %>

<%
Product product = (Product) request.getAttribute("product");
%>

<portlet:renderURL var="cancelURL">
	<portlet:param name="jspPage" value="/view.jsp" />
</portlet:renderURL>

<portlet:actionURL name="updateProduct" var="updateProductURL" />

<h2><<liferay-ui:message key="prodreg-web-admin.edit-product" /></h2>

<aui:form name="fm" action="<%= updateProductURL %>" method="post">
	<aui:fieldset>
		<aui:input name="resourcePrimKey" value="<%= product.getId() %>" type="hidden" />
		<aui:input name="productName" label="prodreg-web-admin.product-name" value="<%= product.getName() %>" />
		<aui:input name="productSerial" label="prodreg-web-admin.product-serial" value="<%= product.getSerialNumber() %>" />
		<aui:button-row>
			<aui:button type="submit" />
			<aui:button type="cancel" onClick="<%= cancelURL %>" />
		</aui:button-row>
	</aui:fieldset>
</aui:form>
