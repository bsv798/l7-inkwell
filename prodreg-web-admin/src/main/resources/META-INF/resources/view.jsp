<%@ include file="init.jsp" %>

<portlet:actionURL name="addProduct" var="addProductURL" />

<aui:form action="<%= addProductURL.toString() %>" method="post">
	<aui:fieldset>
		<aui:input name="productName" label="prodreg-web-admin.product-name" />
		<aui:input name="productSerial" label="prodreg-web-admin.product-serial" />
		<aui:button-row>
			<aui:button type="submit" />
		</aui:button-row>
	</aui:fieldset>
</aui:form>

<liferay-ui:search-container emptyResultsMessage="prodreg-web-admin.no-products" delta="5">
	<liferay-ui:search-container-results>
		<%
		List<Product> tempResults = ActionUtil.getProductsFromRequest(renderRequest);
		
		searchContainer.setTotal(tempResults.size());
		searchContainer.setResults(ListUtil.subList(tempResults, searchContainer.getStart(), searchContainer.getEnd()));
		%>
	</liferay-ui:search-container-results>
	
	<liferay-ui:search-container-row className="com.inkwell.service.model.Product"
		keyProperty="id" modelVar="product">
		<liferay-ui:search-container-column-text name="prodreg-web-admin.product-name" property="name" />
		<liferay-ui:search-container-column-text name="prodreg-web-admin.product-serial" property="serialNumber" />
		<liferay-ui:search-container-column-jsp name="prodreg-web-admin.product-actions" path="/actions.jsp" align="right" />
	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator />
</liferay-ui:search-container>
