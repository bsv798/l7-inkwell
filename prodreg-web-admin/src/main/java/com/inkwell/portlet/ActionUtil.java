package com.inkwell.portlet;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.PortletRequest;

import com.inkwell.service.model.Product;
import com.inkwell.service.model.impl.ProductImpl;
import com.inkwell.service.service.ProductLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;

public class ActionUtil {
	
	public static Product getProductFromRequest(PortletRequest request) {
		ThemeDisplay themeDisplay = getThemeDisplay(request);
		Product product = new ProductImpl();
		
		product.setCompanyId(themeDisplay.getCompanyId());
		product.setGroupId(themeDisplay.getSiteGroupId());
		product.setId(ParamUtil.getLong(request, "resourcePrimKey"));
		product.setName(ParamUtil.getString(request, "productName"));
		product.setSerialNumber(ParamUtil.getString(request, "productSerial"));
		
		return product;
	}
	
	public static List<Product> getProductsFromRequest(PortletRequest request) {
		ThemeDisplay themeDisplay = getThemeDisplay(request);
		List<Product> products = new ArrayList<>();
		
		try {
			products = ProductLocalServiceUtil.getAllProductsByGroupId(themeDisplay.getSiteGroupId());
		} catch (SystemException e) {
			// TODO: handle exception
		}
		
		return products;
	}

	public static ThemeDisplay getThemeDisplay(PortletRequest request) {
		return (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}
}
