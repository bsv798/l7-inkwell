package com.inkwell.portlet;

import java.util.List;

import com.inkwell.service.model.Product;
import com.liferay.portal.kernel.util.Validator;

public class ValidationUtil {
	
	public static boolean validateProduct(Product product, List<String> errors) {
		if (Validator.isNull(product.getName())) {
			errors.add("product-name-required");
		}
		
		if (Validator.isNull(product.getSerialNumber())) {
			errors.add("serial-number-prefix-required");
		}
		
		if (Validator.isNull(product.getCompanyId())) {
			errors.add("missing-company-id");
		}
		
		if (Validator.isNull(product.getGroupId())) {
			errors.add("missing-group-id");
		}
		
		return errors.isEmpty();
	}
}
