package com.inkwell.portlet;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import com.inkwell.constants.ProdregAdminPortletKeys;
import com.inkwell.service.model.Product;
import com.inkwell.service.service.ProductLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;

import javax.portlet.ProcessAction;

@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.add-default-resource=true",
		"com.liferay.portlet.display-category=category.hidden",
		"com.liferay.portlet.layout-cacheable=true",
		"com.liferay.portlet.private-request-attributes=false",
		"com.liferay.portlet.private-session-attributes=false",
		"com.liferay.portlet.render-weight=50",
		"com.liferay.portlet.use-default-template=true",
		"javax.portlet.display-name=prodreg-web-admin Portlet",
		"javax.portlet.expiration-cache=0",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + ProdregAdminPortletKeys.ProdregAdmin,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user",
		"javax.portlet.supports.mime-type=text/html"
	},
	service = Portlet.class
)
public class ProdregAdminPortlet extends MVCPortlet {
	public void addProduct(ActionRequest request, ActionResponse response) throws PortalException {
		ThemeDisplay themeDisplay = ActionUtil.getThemeDisplay(request);
		Product product = ActionUtil.getProductFromRequest(request);
		List<String> errors = new ArrayList<>();
		
		if (ValidationUtil.validateProduct(product, errors)) {
			ProductLocalServiceUtil.addProduct(product, themeDisplay.getUserId());
			SessionMessages.add(request, "product-saved-successfully");
		} else {
			SessionErrors.add(request, "fields-required");
		}
	}

	public void editProduct(ActionRequest request, ActionResponse response) throws PortalException {
		long id = ParamUtil.getLong(request, "resourcePrimKey");
		Product product = ProductLocalServiceUtil.getProduct(id);
		
		request.setAttribute("product", product);
		response.setRenderParameter("jspPage", "/edit.jsp");
	}

	public void deleteProduct(ActionRequest request, ActionResponse response) throws PortalException {
		long id = ParamUtil.getLong(request, "resourcePrimKey");
		
		ProductLocalServiceUtil.deleteProduct(id);
	}

	public void updateProduct(ActionRequest request, ActionResponse response) {
		Product product = ActionUtil.getProductFromRequest(request);
		List<String> errors = new ArrayList<>();
		
		if (ValidationUtil.validateProduct(product, errors)) {
			ProductLocalServiceUtil.updateProduct(product);
			SessionMessages.add(request, "product-updated-successfully");
		} else {
			SessionErrors.add(request, "fields-required");
		}
	}
}
