package com.inkwell.application.list;

import com.inkwell.constants.ProdregAdminPanelCategoryKeys;
import com.inkwell.constants.ProdregAdminPortletKeys;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=100",
		"panel.category.key=" + ProdregAdminPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class ProdregAdminPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return ProdregAdminPortletKeys.ProdregAdmin;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + ProdregAdminPortletKeys.ProdregAdmin + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}
